#####****Note: Run each from their specific base directories.


# Challenge 1

API Service to return <sha_256> encrypted string for any input string <msg> and vice versa if <msg> was seen once before.

#####Service can be reached at: 

    	Host address: https://68cbc5bc.ngrok.io


Note: I'm currently using a free service so for first-time usage to anyone of the endpoints give it a second to breathe. If you try it locally you'll see it runs perfectly fast and fine :) 

#####Format: 

1. POST Request:

		curl -X POST -H "Content-Type: application/json" -d '{"message": <msg_string>}' http://<host_ip>/messages
	  Return:

		{
    		"digest": <sha_256_string>
		}

2. GET Request:

		curl -i http://<host_ip>/messages/<sha_256>
	  Return: 

		HTTP/1.1 200 OK
		Connection: close
		Date: Tue, 25 Sep 2018 09:17:50 GMT
		Server: gunicorn/19.9.0
		content-length: 22
		content-type: application/json; charset=UTF-8

		{
			"message": <input_string>
		}

#####Example: 

1. POST Request:

		curl -X POST -H "Content-Type: application/json" -d '{"message": "foo"}' http://127.0.0.1:8000/messages
	  Return:

		{
			"digest": "d556a4aa42b04d56ad5a12052fcd09a155a3f9699c26ef1c309cde21095acbf4"
		}
		
2. GET Request:

		curl -i http://127.0.0.1:8000/messages/2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae
	  Return:

	    HTTP/1.1 200 OK
	    Connection: close
		Date: Tue, 25 Sep 2018 09:17:50 GMT
		Server: gunicorn/19.9.0
		content-length: 22
		content-type: application/json; charset=UTF-8

		{
			"message": "foo"
		}

#####Run Test:

		python -m pytest tests/

		
##### TODO: Things to add in future when we can spend more time on this and extend it further:

1. Switch to using Postgres or other database like Mongodb.
2. Use Logger or other Logging packages for consistency and cleaner logging.
3. Modularize the code further with Models, Literals and Configs.

4. Deploy to paid public cloud provider like: AWS/Heroku/GCP (I'm using a free service for now)
