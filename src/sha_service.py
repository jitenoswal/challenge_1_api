import json
import falcon
import hashlib
import sys
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy import Table
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy.exc import IntegrityError

"""
    Class to handle routes and dependent functions of ShaService APIs
"""


class ShaService(object):

    """
        Class Object Initialization
        `tablename` is name of the table in db which has schema (msg, sha256)
        `URL` is the name of the db
        `engine` is the type of db-engine
    """
    def __init__(self):
        self._tablename = 'sha256_msg_db_store'
        self._URL = 'sqlite:///' + self._tablename + '.db'
        self._engine = create_engine(self._URL)

    """
        Handles POST route "/message" body={'message': <msg_string>}
        `msg_string` is post-body msg string which we need `sha256` encoded string
        Returns a response.status=200 & `sha256` encoded string
    """
    def on_post(self, req, resp):
        print ('[POST] Request is being processed: ' + req.media.get('message'))
        sha_256 = self.encode_sha256_db(req.media.get('message'))
        digest = { 'digest': sha_256 }
        resp.body = json.dumps(digest, ensure_ascii=False)
        resp.status = falcon.HTTP_200

    """
        Handles GET route "/message/<sha_256_string>" 
        `sha_256_string` is sha256 string which we need to decode if it was already seen by service and return `msg`
        Returns if exist: response.status=200 & `msg` decoded string
                else: response.status=404 & `err_msg` = Message not found 
    """
    def on_get(self, req, resp, sha_256):
        print ('[GET] Request is being processed: ' + sha_256)
        msg = self.decode_sha256_db(sha_256)
        if msg:
            message = {'message':msg}
            print ('[GET Type Resp.Media]: ' + str(type(message)))
            print ('[GET Resp.Media]: ' + str(message))
            resp.body = json.dumps(message, ensure_ascii=False)
            resp.status = falcon.HTTP_200
        else:
            err_msg = { 'err_msg': 'Message not found' }
            resp.body = json.dumps(err_msg, ensure_ascii=False)
            resp.status = falcon.HTTP_404

    """
        Function to encode `msg` to return `sha256` and store them in sqlite3:db
        `msg` is input message string which needs encoding
        Returns:
            if db_store working correctly return `sha256` is the encoded string after saving it in db
            else: None
    """
    def encode_sha256_db(self, msg):
        sha_256 = hashlib.sha256(msg.encode('UTF-8')).hexdigest()
        try:
            connection = self._engine.connect()
            table = self.get_table_connection()
            if table != None:
                ins = table.insert().values(msg=msg, sha256=sha_256)
                connection.execute(ins)
                return sha_256
            return None
        except IntegrityError as e:
            print ("[Info]: sqlite3.IntegrityError: Non-Unique insert. ")
            return sha_256
        except:
            print("[Error]: Unexpected error:", sys.exc_info())
            return None

    """
        Function to decode `sha_256` and return corresponding `msg` if it exist in db store
        `sha_256` is sha256 encoded string which needs decoding
        Returns:
            if db_store working correctly and `sha256` exists in it return `msg`
            else: None
    """
    def decode_sha256_db(self, sha_256):
        try:
            connection = self._engine.connect()
            table = self.get_table_connection()
            if table != None:
                select_st = table.select().where(table.c.sha256 == sha_256)
                res = connection.execute(select_st)
                for _row in res:
                    pass
                return _row[0]
            return None
        except UnboundLocalError as e:
            print ("[Info]: Sha256 doesn't exist in db store. Unseen message")
        except:
            print("[Error]: Unexpected error:", sys.exc_info()[0])
            return None

    """
        Function to create table with schema:
            Column('msg', String, nullable=False),
            Column('sha256', String, primary_key=True)
        Returns:
            meta which has table structure in it.
    """
    def create_table(self):
        meta = MetaData(self._engine)

        table = Table(self._tablename, meta,
                      Column('msg', String, nullable=False),
                      Column('sha256', String, primary_key=True))

        meta.create_all()
        return meta

    """
        Function to validate if db & table exists if not create db & table
        Returns:
            sha_service data store table context to be used for storing and retriving msg, sha256 pair
    """
    def get_table_connection(self):
        try:
            meta = MetaData()
            meta.reflect(self._engine)
            table = meta.tables[self._tablename]
            return table
        except KeyError:
            meta = self.create_table()
            table = meta.tables[self._tablename]
            return table
        except:
            print("[Error]: Unexpected error:", sys.exc_info())
            return None

