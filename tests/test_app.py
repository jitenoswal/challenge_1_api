import falcon
from falcon import testing
import json
import pytest

from src.app import create_app


@pytest.fixture
def client():
    return testing.TestClient(create_app())


"""
    API Test for testing get_msg endpoint
    Endpoint: GET 
    Route: /messages 
"""
def test_get_messages(client):
    valid_doc = {u'message': u'jiten'}
    response = client.simulate_get('/messages/519301f9e55cb6323b2c6f841d71b5e34af659664e658d627ea629c5f0899351')
    result_doc = json.loads(response.content, encoding='UTF-8')
    assert result_doc == valid_doc
    assert response.status == falcon.HTTP_OK


"""
    Test for testing post_message endpoint
    Endpoint: POST 
    Route: /messages 
"""
def test_post_messages(client):
    valid_doc = {u'digest': u'519301f9e55cb6323b2c6f841d71b5e34af659664e658d627ea629c5f0899351'}
    msg = {'message': 'jiten'}
    response = client.simulate_post('/messages', body=json.dumps(msg))
    result_doc = json.loads(response.content, encoding='UTF-8')
    assert response.status == falcon.HTTP_200
    assert result_doc == valid_doc
