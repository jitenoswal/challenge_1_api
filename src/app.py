import falcon

from .sha_service import ShaService

api = application = falcon.API()

"""
    Function to setup falcon.API instances as callable WSGI apps.
    Also, registering routes for our service:
        `/messages` body={msg: <msg_string>}
        `/messages/<sha256_string>`
"""
def create_app():
    api = falcon.API()
    sha_service = ShaService()
    api.add_route('/messages', sha_service)
    api.add_route('/messages/{sha_256}', sha_service)

    return api


def get_app():
    return create_app()


